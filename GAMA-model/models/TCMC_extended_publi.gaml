model TCMC

global {

//  ####################################
//  ***** General model parameters *****
//  ####################################


// Starting year
	int startingYear <- 2010;
	// Current year
	int currentYear <- startingYear;
	// Skip to next year at the end of every commute cycle ?
	bool skipYearToggle <- true;
	// Day number, is increased by 1 whenever Commuters finish a cycle. Start each year at 6 am, 0 minutes.
	int day <- 1;
	int hour <- 6;
	int minute <- 0;
	// Store the cycle number at which there is a day reset or year reset.
	int cycleN_lastReset;
	// If true, disable visualisation effects to massively increase computational speed.
	bool highPerformanceMode <- false;
	// Model X seconds per tick.
	float secondsPerTick <- 10.0;

	//  ######################################
	//  ***** Prediction mode parameters *****
	//  ######################################

	// If true, use instructions from PDC to generate buildings. If false, use real life data from startingYear to most recent data available.
	bool predictionMode;
	// Count how many commuters are removed from the destruction of buildings inside "Densification" areas.
	float nCommutersRemoved <- 0.0;
	// Size of the buildings to generate (squares by default).
	float newBuildingsSize;
	// Number of new buildings to construct in the area per year and per hectare (100mx100m = 10k m2) of the PDC area.
	float densificationValue;
	// Number of square meters per person
	float areaPerPerson;
	// Number of stories per building
	int nStories;
	// Number of commuters per building
	float nCommutersPerBuilding;
	// Percentage of new commerces built among all the new buildings inside an "activités" PDC zone.
	float newCommercesProportion;
	// Percentage of housing built inside "Renouvellement mixte" PDC areas.
	float mixteHousingProportion;

	//  ###########################################
	//  ***** Files and world parametrization *****
	//  ###########################################

	// Load shapefiles for road and tram network, population info, and building info.
	// Polylines from SHP for tram rail network need to be split up beforehand (tool "Split Line at Point").
	// for instance with respect to the stops SHP.
	file roadNetworkSHP <- file("../includes/clippedLayers/Clip_GMO_GRAPHE_ROUTIER.shp");
	file tramNetworkSHP <- file("../includes/clippedLayers/Clip_TPG_TRAMS_Split.shp");
	file tramStopsSHP <- file("../includes/clippedLayers/Clip_TPG_ARRETS_14_18.shp");
	file addressesSHP <- file("../includes/clippedLayers/Clip_OCS_POPULATION_ADRESSE.shp");
	file buildingsSHP <- file("../includes/clippedLayers/Clip_CAD_BATIMENT_HORSOL.shp");
	file PDCSHP <- file("../includes/clippedLayers/Clip_PDC.shp");
	file plotsSHP <- file("../includes/clippedLayers/Clip_Parcelles.shp");

	// Set world boundaries. Warning, this HAS to be done in global BEFORE init, otherwise everything breaks.
	// Which means that the envelope-defining file also needs to be defined in global before init.
	geometry shape <- envelope(roadNetworkSHP);

	// Initialize road and tram rail network graphs.
	graph roadNetwork;
	graph tramRailNetwork;

	//  ##########################################
	//  ***** Buildings and plots parameters *****
	//  ##########################################

	// Select which building corresponds to Cornavin.
	Buildings Cornavin;
	list<Buildings> TravailWithoutCornavin;
	list<Buildings> Commerces;
	list<Buildings> Loisirs;
	list<Buildings> Schools;

	// Store value of the density of the plot with highest density, to update color of plots.
	float maxPlotDensity;

	// Variables used to classify activities.
	file activitiesCSV <- csv_file("../includes/ListNames_CAD_BATIMENT_HORSOL_DESTINATIO_classees.csv", ";");
	matrix activityData;
	list activityList;

	//  ################################
	//  ***** Commuters parameters *****
	//  ################################

	// Amount of people aggregated into a single commuter agent.
	int commuterGranularity <- 10;
	// Commuter demographics
	float childrenProportion <- 0.173;
	float studentsProportion <- 0.064;
	float adultsProportion <- 0.597;
	float retiredProportion <- 0.166;

	// Ask commuters to take tram or not.
	bool takeTramToggle <- true;
	// Threshold above which the commuters take the car instead of the tram.
	float distanceToStopThreshold <- 400.0;
	// Proportion of adults that do not take the tram no matter what.
	float noTramAdultProportion;

	//  ############################
	//  ***** Trams parameters *****
	//  ############################

	// Tram speed, meters per tick, averaged between lines 14 and 18.
	float tramSpeed <- 5.2 * secondsPerTick;
	// How many vehicles for each tram line.
	// Base values : at peak hour, 1 tram every 4 minutes for line 14, one whole loop Meyrin <-> Cornavin <-> Meyrin takes 44 minutes, so 44/4 = 11.
	// Base values : at peak hour, 1 tram every 10 minutes for line 18, one whole loop CERN <-> Cornavin <-> CERN takes 48 minutes, so 48/10 = 5.
	int nTrams14 <- 7;
	int nTrams18 <- 5;
	map nTramsMap <- [14::nTrams14, 18::nTrams18];

	//  ############################
	//  ***** Stops parameters *****
	//  ############################

	// Store a dictionary where keys are tram line numbers and values a list of the name of the stops (in order) from terminus1 to terminus2.
	map orderedStopsMap;
	// Store a list of all stops aller-retour for each line.
	map orderedStopsMapAR;
	// Store the max distance between stops of the same name.
	float maxHomonymStopSeparation <- 0.0;

	//  ############################
	//  ***** User interaction *****
	//  ############################

	// Location selected by the user with a LMB click.
	point selected_location;
	// This polygon will be defined by the user to select which addresses to model.
	geometry addressContainer;
	// This is the list of points which define the extent of the aforementionned polygon.
	list polygonSummits;
	// Switch polygon display on/off.
	bool drawPolygonBool <- false;

	//  ###############################
	//  ***** Save and load files *****
	//  ###############################

	// Save and load polygons.
	string savedPolygonFilePath <- "../includes/savedPolygon.txt";
	string toSave <- "";
	list toLoad;

	// Save a history of the amount of commuters in the model.
	list populationHistory <- [];
	list yearHistory <- [];
	string savedIndicatorsFilePath <- "../output/savedIndicators.csv";
	string outputRoadsSHPFilePath <- "../output/shp/outputRoads";
	string outputCommutersSHPFP <- "../output/shp/commuters/commuters";
	string commuterDataOutput <- "../output/commuterData/commuterData";
	string tramDataOutput <- "../output/tramData/tramData";
	string roadDataOutput <- "../output/roadData/roadData";

	init {
		write "All shapefiles loaded";

		// Import activity correpsondance data.
		activityData <- matrix(activitiesCSV);
		// Store all activity types into a list.
		activityList <- activityData column_at 0;
		// Configure output CSV header.
		add "Population" to: populationHistory;
		add "Year" to: yearHistory;

		// Create PDC areas.
		create PDC_areas from: PDCSHP with: [type:: string(read("Type"))];
		write "PDC areas created.";

		// Create plots, roads, tram rail network and tram stops.
		create Plots from: plotsSHP;
		write "Plots created.";
		create Roads from: roadNetworkSHP;
		roadNetwork <- as_edge_graph(Roads);
		write "Roads created.";
		create TramRails from: tramNetworkSHP;
		tramRailNetwork <- as_edge_graph(TramRails);
		write "TramRails created.";
		create TramStops from: tramStopsSHP with: [importedTPGLines::string(read("LIGNE")), stopName::string(read("NOM_ARRET")), stopDirection::string(read("DIRECTION"))] {
		// Remove coutance for the moment.
			if stopName contains "Coutance" {
				do die;
			}

		}

		write "Tram stops created.";
		// Build the ordered list of stops for each line. This is mandatory for the passengers to know which direction to go.
		do getOrderedStopsMap;

		// Determine the maximum distance between two stops that have the same name.
		do determineMaxHomonymStopSeparation;

		// Import construction date for buildings, infer it from construction period if not available i.e.
		// Select latest year from construction period. If both info N/A, set date to startingYear.
		create Buildings from: buildingsSHP with:
		[objectID::string(read("NO_BATIMEN")), constructionDate::int(read("ANNEE_CONS")), constructionPeriod::string(read("EPOQUE_CON")), importedActivity::string(read("DESTINATIO")), height::float(read("HAUTEUR"))]
		{
			if constructionDate = 0 {
				if constructionPeriod != nil {
					constructionDate <- int(copy_between(constructionPeriod, 18, 22));
				} else {
					constructionDate <- startingYear;
				}

			}
			// Set color to grey if building not yet built.
			if constructionDate > startingYear {
				built <- false;
				color <- rgb(184, 184, 184);
			}

			// Detect activity types.
			if importedActivity in activityList {
			// Import list of activity types from matrix.
				activityTypes <- activityData row_at (activityList index_of importedActivity);
				// Remove activity (info already contained inside importedActivity).
				remove importedActivity from: activityTypes;
				// Remove blank cells.
				remove all: "" from: activityTypes;
			} else {
				write "Warning ! Activity correspondance error for " + self;
			}

		}

		write "Buildings created.";

		// Create trams.
		create Trams14 number: nTramsMap[14];
		create Trams18 number: nTramsMap[18];
		write "Trams created.";
		// Index of the stops.
		float i <- 0.0;
		ask Trams14 {
		// Get list of stops for this tram line.
			list tempList <- orderedStopsMapAR[self.lineNumber];
			// Go to stop indicated by the current index.
			location <- tempList[round(i)];
			// Increment the value of i (by the amount of stops divided by the number of trams) for the following tram.
			// Each tram performs the actions in this block one after the other.
			i <- i + length(tempList) / nTramsMap[lineNumber];
		}

		i <- 0.0;
		ask Trams18 {
			list tempList <- orderedStopsMapAR[self.lineNumber];
			location <- tempList[round(i)];
			i <- i + length(tempList) / nTramsMap[lineNumber];
		}

		// Cornavin is the only building which has an objectID of "G840".
		Cornavin <- Buildings first_with (each.objectID = "G840");

		// One commuter represents the behavior of one among commuterGranularity (10 by default) citizens.
		create Addresses from: addressesSHP with: [nCommuters::int(read("population")) / commuterGranularity];
		write "Addresses created.";

		// If in prediction mode, destroy all buildings inside "Densification" areas to make space for new ones.
		if predictionMode {
			ask PDC_areas where ("Densification" in each.type) {
				ask Buildings inside self {
					built <- false;
					color <- rgb(64, 64, 64);
				}

				ask Addresses inside self {
					nCommutersRemoved <- nCommutersRemoved + self.nCommuters;
					do die;
				}

			}

			write string(round(nCommutersRemoved)) + " commuters were removed.";
		}

		// Tag commerces and Loisirs.
		TravailWithoutCornavin <- Buildings where (each.activityTypes contains "Travail" and each.objectID != "G840" and each.built = true);
		Commerces <- Buildings where (each.activityTypes contains "Commerce" and each.built = true);
		Loisirs <- Buildings where (each.activityTypes contains "Loisirs" and each.built = true);
		Schools <- Buildings where (each.activityTypes contains "Ecole" and each.built = true);

		// Calculate density.
		ask Plots {
			do calculateDensity;
		}

		maxPlotDensity <- Plots max_of each.density;
		ask Plots {
			do updateColor;
		}

		// Spawn commuters inside Addresses.
		ask Addresses {
			ask Buildings closest_to self {
			// Only create commuters on buildings already built.
				if constructionDate <= startingYear {
				// Account for decimals : with a number of commuters of 5.7, 5 commuters are created, with 70% chance of creating a 6th one.
					create Commuters number: int(myself.nCommuters) + int(flip(myself.nCommuters - int(myself.nCommuters))) {
						home <- myself;
						do setWorkplace;
						do initCommuters;
					}

				}

			}

		}

		write "There are currently " + string(length(Commuters)) + " commuters.";

		//Initialize new save files.
		save "commuterName;nJourney;demoClass;considersTram;transportationMethod;timeSpent;timeSpentInTram;timeSpentWaiting;chosenStopA;chosenStopB;location" to:
		(commuterDataOutput + string(currentYear) + ".txt") type: text header: false;
		save "tramName;tramLine;nPassengers;cycleNo;time;location" to: (tramDataOutput + string(currentYear) + ".txt") type: text header: false;
		save "roadName;listPassages;listCycles" to: (roadDataOutput + string(currentYear) + ".txt") type: text header: false;
		save "year;cycleNo" to: "../output/yearTransitions.txt" type: text header: false;
		save string(currentYear) + ";" + cycle to: "../output/yearTransitions.txt" type: text header: false rewrite: false;
	}

	// ######################################
	// ***** MODEL ACTIONS AND BEHAVIOR *****
	// ######################################

	// One tick represents secondsPerTick (default value is 10) seconds (speeds are in meters per secondsPerTick seconds).
	reflex advanceMinutesHours when: (cycle - cycleN_lastReset) mod (60.0 / secondsPerTick) = 0 and cycle != 0 {
		minute <- minute + 1;
		if minute = 60 {
			minute <- 0;
			hour <- hour + 1;
			if hour = 24 {
				hour <- 0;
			}

		}

	}

	// When all agents get back home, the day ends.
	// This is also where day and time of day reporters are updated, and traffic counters are reset.
	reflex newDay {
		if Commuters all_verify (each.endedDay = true) {
		// Start of a new day. Store cycle at which it happens.
			day <- day + 1;
			hour <- 6;
			minute <- 0;
			cycleN_lastReset <- cycle;
			// New year start
			if skipYearToggle {
				do newYear;
			}

			// Reset some values.
			ask Roads {
				nPassages <- 0;
				listPassages <- [];
				roadsListCycles <- [];
			}

			// Reset relevant commuter attributes.
			ask Commuters {
				endedDay <- false;
				nJourney <- 0;
			}

		}

	}

	// New year : color in newly built buildings, and create commuters inside newly built buildings.
	action newYear {

	// Save relevant commuter attributes
	//save "Name; demoClass; considersTram; takesTram; timeSpent; timeSpentWaiting" to: (commuterDataOutput + string(currentYear) + ".txt") type: text header: false;
	//ask Commuters {
	//save name + ";" + demoClass + ";" + considersTram + ";" + timeSpent + ";" + timeSpentWaiting to: (commuterDataOutput + string(currentYear) + ".txt") type: text rewrite: false
	//header: false;
	//timeSpent <- 0.0;
	//timeSpentWaiting <- 0.0;
	//listLocations <- [];
	//listCycles <- [];
	//}

	// Save values inside file
	//add length(Commuters) to: populationHistory;
	//add currentYear to: yearHistory;
	//save yearHistory to: savedIndicatorsFilePath type: "csv" header: false;
	//save populationHistory to: savedIndicatorsFilePath type: "csv" rewrite: false;
	//ask Roads {
	//do updateTrafficHistory;
	//save trafficHistory to: savedIndicatorsFilePath type: "csv" rewrite: false;
	//}
	// Save road network with traffic info for each year.
		save Roads to: (roadDataOutput + string(currentYear) + ".shp") type: "shp" attributes: ["roadName"::name, "nPassages"::nPassages] crs: "EPSG:2056";
		ask Roads {
			save name + ";" + listPassages + ";" + roadsListCycles to: (roadDataOutput + string(currentYear) + ".txt") type: text header: false rewrite: false;
		}

		// Reset day number, update year number.
		day <- 1;
		currentYear <- currentYear + 1;
		if currentYear = 2023 {
			do pause;
		}

		save string(currentYear) + ";" + cycle to: "../output/yearTransitions.txt" type: text header: false rewrite: false;

		//Initialize new save files.
		save "commuterName;nJourney;demoClass;considersTram;transportationMethod;timeSpent;timeSpentInTram;timeSpentWaiting;chosenStopA;chosenStopB;location" to:
		(commuterDataOutput + string(currentYear) + ".txt") type: text header: false;
		save "tramName;tramLine;nPassengers;cycleNo;time;location" to: (tramDataOutput + string(currentYear) + ".txt") type: text header: false;
		save "roadName;nPassages;cycleNo" to: (roadDataOutput + string(currentYear) + ".txt") type: text header: false;

		// Build new buildings.
		if predictionMode {
		// In prediction mode, buildings are generated inside the areas indicated by the PDC.
			ask PDC_areas where ("habitation" in each.type or "activités" in each.type or "mixte" in each.type) {
			// Create a number of buildings according to the set parameters.
				float nBuildings <- geometry(self).area * densificationValue / (newBuildingsSize ^ 2);
				create Buildings number: int(nBuildings) + int(flip(nBuildings - int(nBuildings))) {
				// Define shape.
					shape <- square(newBuildingsSize);
					// Define height, by default, one story is 3m high.
					height <- 3.0 * nStories;
					// Pick a random location inside the PDC area.
					location <- any_location_in(myself);
					// Mark building as built.
					built <- true;
					// Set construction year.
					constructionDate <- currentYear;
					mixteHousingBool <- flip(mixteHousingProportion);
					// If built inside a PDC area for housing, set color to green.
					if "habitation" in myself.type or ("mixte" in myself.type and self.mixteHousingBool) {
						color <- rgb(98, 240, 169);
						// Create inhabitants. Total building size is size^2 * nStories, each commuter occupies space * commuterGranularity.
						nCommutersPerBuilding <- (newBuildingsSize ^ 2) / areaPerPerson * nStories / commuterGranularity;
						// Account for decimals : with a number of commuters of 5.7, 5 commuters are created, with 70% chance of creating a 6th one.
						create Commuters number: int(nCommutersPerBuilding) + int(flip(nCommutersPerBuilding - int(nCommutersPerBuilding))) {
							home <- myself;
							do setWorkplace;
							do initCommuters;
						}

						// If built inside a PDC area for "activités", set color to purple.
					} else if "activités" in myself.type or ("mixte" in myself.type and not (self.mixteHousingBool)) {
						color <- rgb(125, 4, 68);
						// Mark building as potential workplace.
						activityTypes <- ["Travail"];
						// X% chance of being "Commerce".
						if flip(newCommercesProportion) {
							add "Commerce" to: activityTypes;
						}

					}

				}

			}

		} else {
		// When not in prediction mode, buildings are generated based on info inside the shapefile.
			ask Buildings where (each.constructionDate = currentYear) {
				color <- rgb(222, 196, 144);
				built <- true;
			}
			// Create commuters inside newly built buildings (non prediction mode).
			ask Addresses {
				ask Buildings closest_to self {
				// Create commuters ONLY inside newly built buildings.
					if constructionDate = currentYear {
					// Account for decimals : with a number of commuters of 5.7, 5 commuters are created, with 70% chance of creating a 6th one.
						create Commuters number: int(myself.nCommuters) + int(flip(myself.nCommuters - int(myself.nCommuters))) {
							home <- myself;
							do setWorkplace;
							do initCommuters;
						}

					}

				}

			}

		}

		// Tag commerces and Loisirs.
		TravailWithoutCornavin <- Buildings where (each.activityTypes contains "Travail" and each.objectID != "G840" and each.built = true);
		Commerces <- Buildings where (each.activityTypes contains "Commerce" and each.built = true);
		Loisirs <- Buildings where (each.activityTypes contains "Loisirs" and each.built = true);
		Schools <- Buildings where (each.activityTypes contains "Ecole" and each.built = true);

		// Calculate density.
		ask Plots {
			do calculateDensity;
		}

		maxPlotDensity <- Plots max_of each.density;
		ask Plots {
			do updateColor;
		}

		// End by removing commuters outside the pre-defined perimeter.
		do kill_external_commuters;
	}

	action getOrderedStopsMap {
	// Create one tram per line.
		create Trams14 number: 1;
		create Trams18 number: 1;
		ask agents of_generic_species Trams {
		// Initialize line::direction map for all stops of the line to direction of -1, replace only detected stops to direction 1 later.
			ask TramStops {
				if myself.lineNumber in self.tramLines {
					add myself.lineNumber::-1 to: lineDirectionMap;
				}

			}
			// Define temporary list to store detected stops' names.
			list orderedStopsList;
			// Define temporary list to store detected stops as objects (aller-retour).
			list orderedStopsListAR;
			// Obtenir la liste des arrêts aller-retour pour chaque ligne pour la répartition des trams.
			lastStop <- startingStation1;
			location <- lastStop.location;
			target <- terminalStation1;
			// As long as we haven't reached destination :
			loop while: location != target.location {
			// If we haven't yet registered a stop as object in the AR list, register it. Do it first, so that we don't count the last station (to prevent double counting terminal stations).
				if not (lastStop in orderedStopsListAR) {
					add lastStop to: orderedStopsListAR;
				}
				// Update eligible stops : nearby stops which have the correction direction (same as terminus) and line number (contained inside direction).
				eligibleStops <- (TramStops where ((each.stopDirectionList inter target.stopDirectionList) != [])) at_distance speed;

				// Move along rail network normally, stopping at eligible stop if one is found.
				do moveAlongRailNetwork;
				// If we haven't yet registered a stop name in the regular list (aller simple), register it.
				// Also mark this stop as being in the direction "1".
				if not (lastStop.stopName in orderedStopsList) {
					add lastStop.stopName to: orderedStopsList;
					ask lastStop {
						lineDirectionMap[myself.lineNumber] <- 1;
					}

				}

			}
			// add the tuple (line number, complete ordered stop list) to the map (dictionary) of stops' names (aller simple).
			add lineNumber::orderedStopsList to: orderedStopsMap;

			// And now for the return trip.
			lastStop <- startingStation2;
			location <- lastStop.location;
			target <- terminalStation2;
			// As long as we haven't reached destination :
			loop while: location != target.location {
			// If we haven't yet registered a stop, register it. Do it first, so that we don't count the last station (to prevent double counting terminal stations).
				if not (lastStop in orderedStopsListAR) {
					add lastStop to: orderedStopsListAR;
				}
				// Update eligible stops : nearby stops which have the correction direction (same as terminus) and line number (contained inside direction).
				eligibleStops <- (TramStops where ((each.stopDirectionList inter target.stopDirectionList) != [])) at_distance speed;
				// Move along rail network normally, stopping at eligible stop if one is found.
				do moveAlongRailNetwork;
			}
			// add the tuple (line number, complete ordered stop list) to the map (dictionary) of stops as objects (aller retour).
			add lineNumber::orderedStopsListAR to: orderedStopsMapAR;
			do die;
		}

		write orderedStopsMap;
		write orderedStopsMapAR;
	}

	// Determine, among all pairs of stops, the maximum distance between two stops of the same pair (same name, opposite direction).
	action determineMaxHomonymStopSeparation {
		ask TramStops {
			if (self distance_to (TramStops first_with (each.stopName = self.stopName and each.stopDirection != self.stopDirection))) > maxHomonymStopSeparation {
				maxHomonymStopSeparation <- (self distance_to (TramStops first_with (each.stopName = self.stopName and each.stopDirection != self.stopDirection)));
			}

		}

		write "Maximum distance between stops with the same name is " + string(maxHomonymStopSeparation with_precision 1) + " meters.";
	}

	//  ############################
	//  ***** User interaction *****
	//  ############################

	// Action called by a LMB click from the user, it saves the location where the click happened.
	// Used to mark the location by a red cross and invoked by the following action.
	action get_location {
		selected_location <- #user_location;
	}

	// Add selected location to list of point for polygon building.
	action register_location {
		add selected_location to: polygonSummits;
	}

	// Build polygon from list of summits.
	action build_polygon {
		addressContainer <- polygon(polygonSummits);
	}

	action calculate_distance {
		write string((point(polygonSummits[0]) distance_to point(polygonSummits[1])) with_precision 0) + " meters.";
	}

	// Clear polygon and registered list of points.
	action clear_polygon_points {
		addressContainer <- nil;
		polygonSummits <- [];
	}

	// Save polygon (i.e. list of summits) to txt file.
	action save_polygon {
		loop summit over: polygonSummits {
			toSave <- toSave + ";" + string(summit);
		}

		save toSave to: savedPolygonFilePath type: text;
		write "Polygon successfully saved!";
	}

	// Load polygon (i.e. list of summits) from txt file.
	action load_polygon {
		if file_exists(savedPolygonFilePath) {
			toLoad <- string(text_file(savedPolygonFilePath)) split_with ";";
			polygonSummits <- [];
			loop summit over: toLoad {
				add point(summit) to: polygonSummits;
			}

			addressContainer <- polygon(polygonSummits);
			write "Polygon successfully loaded";
		} else {
			write "File does not exist !";
		}

	}

	// Once polygon is defined, remove all commuters outside zone of interest.
	action kill_external_commuters {
		ask Commuters - Commuters inside addressContainer {
			do die;
		}

		write "There are currently " + string(length(Commuters)) + " commuters.";
		if predictionMode and (currentYear = startingYear) {
			write "There were " + string(round(length(Commuters) + nCommutersRemoved)) + " commuters before building destruction.";
		}

	}

	// Get origin of model as well as direction. It is situated in the top left corner. As x increases we go right, and as y increases we go down.
	action getOrigin {
		ask Trams18 {
			do die;
		}

		create Trams18 {
			location <- {0, 0};
		}

		create Trams18 {
			location <- {100, 100};
		}

		save Trams18 to: "../output/getOrigin/origin.shp" type: shp;
		ask Trams18 {
			do die;
		}

	}

}

//*******************************************//
//************* ROAD NETWORK ****************//
//*******************************************//
species Roads {
// Color the road in red if it has seen at least one commuter.
	rgb defaultColor <- #black //update: rgb(255 * int(nPassages > 0), 0, 0)
;
	// Counter for the amount of different commuters this chunk of road has seen today.
	int nPassages <- 0;
	list roadsListCycles <- [];
	list listPassages <- [];

	// Keep track of each road's total amount of traffic for each year.
	//list trafficHistory <- [];
	init {
	//add self.name to: trafficHistory;
	}

	//action updateTrafficHistory {
	//add self.nPassages to: trafficHistory;
	//}
	aspect base {
		draw shape color: defaultColor;
	}

}

//*******************************************//
//********* TRAM RAILROAD NETWORK ***********//
//*******************************************//
species TramRails {
	rgb color <- #blue;
	// Imported attributes from SHP.
	string tramLine;
	string tramDirection;

	aspect base {
		draw shape color: color;
	}

}

//*******************************************//
//************ TRAMS - VEHICLES *************//
//*******************************************//
species Trams skills: [moving] {
// 2 terminal stations, and 2 associated starting stations.
	TramStops startingStation1;
	TramStops terminalStation1;
	TramStops startingStation2;
	TramStops terminalStation2;
	// Color of the tram.
	rgb color;
	// Target for movement alongside rail network.
	TramStops target;
	// Used for first target selection.
	float TS1pathLength <- 0.0;
	float TS2pathLength <- 0.0;

	// Store line number.
	int lineNumber;
	// Keep track of tram's direction (also registers tram line).
	pair lineDirection;
	// Store last stop visited.
	TramStops lastStop;
	// Keep track of eligible stops
	list<TramStops> eligibleStops;

	// Maximum capacity of trams, for trams 14 and 18, max capacity is 237 or 261 people. In terms of commuters, this depends on how many people represents one commuter.
	int maxCapacity <- int(249.0 / commuterGranularity);
	// Current amount of passengers
	int nPassengers <- 0;
	int lastnPassengers <- 0;
	// Border color changes to red whenever tram is full.
	rgb borderColor <- #black;

	// Initial set up action called by the init{} of each subspecies (Trams14 and Trams18).
	action setUpTrams {
		speed <- tramSpeed;
	}

	// Trams gain a red border when full.
	reflex updateBorderColor when: not (highPerformanceMode) {
		if nPassengers = maxCapacity {
			borderColor <- #red;
		} else {
			borderColor <- #black;
		}

	}

	reflex updateEligibleStops when: target != nil {
		eligibleStops <- (TramStops where ((each.stopDirectionList inter target.stopDirectionList) != [])) at_distance speed;
	}

	reflex move {
	// Target selection at setup.
		if target = nil {
		// Measure the length of the paths to the two terminal stations (TS) along the rail network.
			loop segment over: (tramRailNetwork path_between (location, terminalStation1.location)).segments {
				TS1pathLength <- TS1pathLength + segment.perimeter;
			}

			loop segment over: (tramRailNetwork path_between (location, terminalStation2.location)).segments {
				TS2pathLength <- TS2pathLength + segment.perimeter;
			}

			// Select closest TS (i.e. with lowest length computed above) as initial target.
			if TS1pathLength < TS2pathLength {
				target <- terminalStation1;
				lineDirection <- lineNumber::1;
			} else {
				target <- terminalStation2;
				lineDirection <- lineNumber::-1;
			}

		}

		// If reached target, select the other TS as new target, and move to corresponding starting location.
		if location = target.location {
			if target = terminalStation1 {
				lastStop <- startingStation2;
				location <- lastStop.location;
				target <- terminalStation2;
				lineDirection <- lineNumber::-1;
			} else {
				lastStop <- startingStation1;
				location <- lastStop.location;
				target <- terminalStation1;
				lineDirection <- lineNumber::1;
			}

		}

		do moveAlongRailNetwork;
	}

	reflex exportTrams when: nPassengers != lastnPassengers {
		save name + ";" + lineNumber + ";" + nPassengers + ";" + cycle + ";" + (string(hour) + ":" + string(minute)) + ";" + location to:
		(tramDataOutput + string(currentYear) + ".txt") format: text rewrite: false header: false;
		lastnPassengers <- nPassengers;
	}

	// Move alongside rail network if no eligible stop nearby, or closest eligible stop has already been visited (departing from it).
	// Or teleport to nearest eligible (correct direction) stop if close enough to it.
	// Eligible stops : check if line number is correct, if direction is correct (same as target terminal station) and in range (equal to speed).
	action moveAlongRailNetwork {
		if empty(eligibleStops) or (eligibleStops contains lastStop) {
			do goto target: target on: tramRailNetwork;
		} else {
			lastStop <- eligibleStops closest_to self;
			location <- lastStop.location;
			// If tram is not full, let passengers evaluate if the tram is compatible with their journey.
			if nPassengers < maxCapacity {
				do triggerEvaluation;
			}

		}

	}

	// Prompt all passengers waiting at current stop to evaluate if the tram can be taken or not.
	// If yes, then chosen tram is set to self, insideTram is set to true, and the chosen tram has its passenger count increased by 1 (for each passenger that went inside).
	action triggerEvaluation {
		ask Commuters where (each.takesTram = true and each.insideTram = false and self.lineDirection in each.chosenDirections.pairs) at_distance (maxHomonymStopSeparation + 1.0) {
		// Check again if tram is not full.
			if myself.nPassengers < myself.maxCapacity {
				chosenTram <- myself;
				insideTram <- true;
				ask myself {
					nPassengers <- nPassengers + 1;
				}

			}

		}

	}

	aspect base {
		draw triangle(40) color: color border: borderColor;
	}

}

species Trams18 parent: Trams {

// CERN <==> Gare Cornavin, 7200m, 23min.
// 5.22 m/s or 52.2 m / 10s.

// Terminal and starting stations are selected based on info contained in SHP (attributes NAME and DIRECTION).
	init {
		do setUpTrams;
		lineNumber <- 18;
		color <- rgb(184, 73, 178);
		startingStation1 <- TramStops first_with (each.stopName = "CERN" and each.stopDirection contains "Palettes");
		terminalStation1 <- TramStops first_with (each.stopName = "Gare Cornavin" and each.stopDirection contains "Palettes");
		startingStation2 <- TramStops first_with (each.stopName = "Gare Cornavin" and each.stopDirection contains "CERN");
		terminalStation2 <- TramStops first_with (each.stopName = "CERN" and each.stopDirection contains "CERN");
	}

}

species Trams14 parent: Trams {

// Meyrin-Gravière <==> Gare Cornavin, 6500m, 21min.
// 5.16 m/s or 51.6 m / 10s.
// Terminal and starting stations are selected based on info contained in SHP (attributes NAME and DIRECTION).
	init {
		do setUpTrams;
		lineNumber <- 14;
		color <- rgb(89, 64, 179);
		startingStation1 <- TramStops first_with (each.stopName = "Meyrin-Gravière" and each.stopDirection contains "Bernex-Vailly");
		terminalStation1 <- TramStops first_with (each.stopName = "Gare Cornavin" and each.stopDirection contains "Bernex-Vailly");
		startingStation2 <- TramStops first_with (each.stopName = "Gare Cornavin" and each.stopDirection contains "Meyrin-Gravière");
		terminalStation2 <- TramStops first_with (each.stopName = "Meyrin-Gravière" and each.stopDirection contains "Meyrin-Gravière");
	}

}

//*******************************************//
//************* TRAMS - STOPS ***************//
//*******************************************//
species TramStops {
	rgb color <- #blue;
	// Imported attributes from SHP.
	string stopName;
	string stopDirection;
	list stopDirectionList;
	string importedTPGLines;
	// List of tram lines using this stop.
	list tramLines;
	// Map Line/Direction
	map lineDirectionMap;

	init {
		if importedTPGLines contains "18" {
			add 18 to: tramLines;
		}

		if importedTPGLines contains "14" {
			add 14 to: tramLines;
		}

		// stopDirection initially contains a list of all lines going through this stop, which is superfluous and needs to be removed for the next operation.
		// We also remove the external parentheses.
		stopDirection <- copy_between(stopDirection, length(importedTPGLines) + 2, length(stopDirection) - 1);
		// Put each couple lineNumber/stopDirection inside a list.
		stopDirectionList <- stopDirection split_with ",";
	}

	aspect base {
		draw circle(10) color: color;
	}

}

//*******************************************//
//*************** BUILDINGS *****************//
//*******************************************//
species Buildings {
	rgb color <- rgb(222, 196, 144);
	// Variable used to store object ID from shapefile, which will help identify Cornavin.
	string objectID;
	int constructionDate;
	string constructionPeriod;
	bool built <- true;
	// Bool that determines whether the building built inside a "Renouvellement mixte" area is of type housing or not.
	bool mixteHousingBool;
	// Building height in meters.
	float height;

	// Variables used to detect and store activity types.
	// Warning : this was set by default to "Centre commercial" and was removed. Not sure if important.
	string importedActivity;
	list activityTypes;

	aspect base {
		draw shape color: color;
	}

}

//***************************************//
//*************** PLOTS *****************//
//***************************************//
species Plots {
	float density;
	list<Buildings> buildingsOn;
	float volumeSum <- 0.0;
	rgb color;

	action calculateDensity {
		volumeSum <- 0.0;
		// Get volume of each building on the plot (multiply surface by height)
		buildingsOn <- Buildings inside self where (each.built);
		loop build over: buildingsOn {
			volumeSum <- volumeSum + build.height * geometry(build).area;
		}
		// Divide volume by plot's area to get density.
		density <- volumeSum / geometry(self).area;
	}

	action updateColor {
		color <- rgb(255, 255 - 255 * density / maxPlotDensity, 255 - 255 * density / maxPlotDensity);
	}

	aspect base {
		draw shape color: #white border: rgb(235, 224, 197);
	}

	aspect colormap {
		draw shape color: color;
	}

}

//*******************************************//
//*************** PDC AREAS *****************//
//*******************************************//
species PDC_areas {
	rgb color;
	string type;

	init {
		if "habitation" in type {
		// green if housing.
			if "Densification" in type {
				color <- rgb(132, 153, 137);
			} else {
				color <- rgb(215, 250, 224);
			}

		} else if "activités" in type {
		// pink if activites.
			color <- rgb(250, 215, 244);
		} else if "mixte" in type {
		// orange if renouvellement mixte.
			color <- rgb(255, 131, 89);
		} else {
		// light yellow otherwise.
			color <- rgb(255, 254, 222);
		} }

	aspect base {
		draw shape color: color;
	} }

	//*******************************************//
//*************** ADDRESSES *****************//
//*******************************************//
species Addresses {
	float nCommuters <- 0.0;

	aspect base {
		draw shape color: #black;
	}

}

//*******************************************//
//*************** COMMUTERS *****************//
//*******************************************//
species Commuters skills: [moving] {

	aspect base {
		draw square(10) color: color;
	}

	Buildings home;
	Buildings workplace;

	// Keep track of commuters' journey number.
	int nJourney <- 0;
	// Keep track of commuters who have ended their day. If all verify true, a new day begins.
	bool endedDay <- false;
	// Keep track of the last road the agent has been seen on.
	geometry lastEdge;

	// Decide whether or not to take the tram.
	bool takesTram <- false;
	// if this is set to false, the commuter never takes the tram.
	bool considersTram;
	// Store transportation method.
	string transportMethod;

	// Start and endpoints of journey.
	Buildings pointA;
	Buildings pointB;

	// List all stops at distance *threshold* (default value is 400.0 m) or less.
	list<TramStops> closestStopsListA;
	list<TramStops> closestStopsListB;
	// Select stops for journey on tram.
	TramStops chosenStopA;
	TramStops chosenStopB;
	// Keep track of total distance to stops (currently bird's flight).
	float totalDistanceToStops <- distanceToStopThreshold * 2.0;
	// Tram lines to take
	list chosenTramLines;
	// which direction should I go ?
	map chosenDirections;
	// Tram taken
	Trams chosenTram;
	// Mark whether or not inside a tram.
	bool insideTram <- false;

	// When to do what ?
	int chosenMinute <- rnd(59);
	int timeToGoWork;
	int timeToLeaveWork;
	int timeToLeaveHome;
	int timeToGoHome <- -1;
	bool idle <- true;
	bool endedJourney <- false;

	// Keep track of total time spent in transports.
	float timeSpent <- 0.0;
	float timeSpentWaiting <- 0.0;
	float timeSpentInTram <- 0.0;

	// Keep track of last destination target.
	string lastTarget <- "home";

	// Keep track of all locations
	//list listLocations;
	//list listCycles;

	// demographic class of commuter (child, student, adult, retired).
	string demoClass;
	rgb color;

	// Initialize commuters
	// Set speed, select optimal starting and destination stops.
	action initCommuters {
	// Speed when taking a car is 20 km/h average, i.e. 5.5 m/s or 55 m/10s.
		speed <- 5.5 * secondsPerTick;
		pointA <- home;
		location <- home.location;
		considersTram <- takeTramToggle;

		// Choose demographic class based on proportions for each class.
		demoClass <- rnd_choice(["child"::childrenProportion, "student"::studentsProportion, "adult"::adultsProportion, "retired"::retiredProportion]);
		do setUpActionPlan;
		if demoClass = "child" {
			color <- #green;
		} else if demoClass = "student" {
			color <- #blue;
		} else if demoClass = "adult" {
			color <- #red;
			if flip(noTramAdultProportion) {
				considersTram <- false;
			}

		} else if demoClass = "retired" {
			color <- #black;
		} }

	action setUpActionPlan {
		if demoClass = "adult" {
			timeToGoWork <- rnd(6, 8);
			timeToLeaveWork <- 10 + timeToGoWork;
		} else if demoClass = "retired" {
			timeToLeaveHome <- rnd(8, 20);
		}

	}

	action setWorkplace {
		if flip(0.5) {
			workplace <- Cornavin;
		} else {
			workplace <- one_of(TravailWithoutCornavin);
		}

	}

	reflex chooseAction_children when: idle and demoClass = "child" {
		if hour = 7 and minute = chosenMinute and lastTarget != "school" {
			pointB <- Schools closest_to home;
			do chooseAction_nested;
			lastTarget <- "school";
		} else if hour = 17 and minute = chosenMinute and lastTarget = "school" {
			if flip(0.5) {
				pointB <- Loisirs closest_to self;
				if pointA != pointB {
					do chooseAction_nested;
				} else {
					timeToGoHome <- hour + 2;
				}

				lastTarget <- "loisir";
			} else {
				pointB <- home;
				do chooseAction_nested;
				lastTarget <- "home";
			}

		} else if hour = timeToGoHome and minute = chosenMinute and lastTarget != "home" {
			pointB <- home;
			do chooseAction_nested;
			timeToGoHome <- -1;
			lastTarget <- "home";
		} }

	reflex chooseAction_students when: idle and demoClass = "student" {
		if hour = 7 and minute = chosenMinute and lastTarget != "uni" {
			pointB <- Cornavin;
			do chooseAction_nested;
			lastTarget <- "uni";
		} else if hour = 17 and minute = chosenMinute and lastTarget = "uni" {
			pointB <- home;
			do chooseAction_nested;
			lastTarget <- "home";
		}

	}

	reflex chooseAction_adults when: idle and demoClass = "adult" {
		if hour = timeToGoWork and minute = chosenMinute and lastTarget != "work" {
			pointB <- workplace;
			do chooseAction_nested;
			lastTarget <- "work";
		} else if hour = timeToLeaveWork and minute = chosenMinute and lastTarget = "work" {
			pointB <- home;
			do chooseAction_nested;
			lastTarget <- "home";
		}

	}

	reflex chooseAction_retired when: idle and demoClass = "retired" {
		if hour = timeToLeaveHome and minute = chosenMinute and lastTarget = "home" {
		// 20% chance to Commerces, 80%*75% = 60% chance to Loisirs, 80%*25% = 20% to stay home.
			if flip(0.2) {
				pointB <- Commerces closest_to self;
				// Only trigger a journey if destination is different from start point.
				if pointA != pointB {
					do chooseAction_nested;
				} else {
				// If no journey is required, we still need to update the time to go home.
					timeToGoHome <- hour + 1;
				}

				lastTarget <- "commerce";
			} else if flip(0.75) {
				pointB <- Loisirs closest_to self;
				if pointA != pointB {
					do chooseAction_nested;
				} else {
					timeToGoHome <- hour + 2;
				}

				lastTarget <- "loisir";
			} else {
				endedDay <- true;
			}

		} else if hour = timeToGoHome and minute = chosenMinute and lastTarget != "home" {
			pointB <- home;
			do chooseAction_nested;
			timeToGoHome <- -1;
			lastTarget <- "home";
		}

	}

	action chooseAction_nested {
		do selectTram;
		do updateTakeTramDecision;
		endedJourney <- false;
		idle <- false;
	}

	// Decide whether to take the tram or not, based on user decision and whether or not it is convenient.
	// i.e. tram stops close enough and there exists a line that service both stops.
	action updateTakeTramDecision {
	// If distance between both points is less than twice the threshold + maxHomonymStopSeparation + 1, then people prefer to go by foot instead of taking the tram.
		if considersTram and pointA distance_to pointB < 2 * distanceToStopThreshold + maxHomonymStopSeparation + 1 {
			takesTram <- false;
			transportMethod <- "foot";
			speed <- 1.2 * secondsPerTick;
			// If conditions are met, they take the tram.
		} else if considersTram and chosenStopA != nil and chosenStopB != nil and chosenStopA.stopName != chosenStopB.stopName {
			takesTram <- true;
			transportMethod <- "tram";
		} else { // Finally, if all else fails, they take the car.
			takesTram <- false;
			transportMethod <- "car";
			speed <- 5.5 * secondsPerTick;
		}

	}

	// Main reflexes which will prompt the commuters to move along the road network.
	// If commuter has arrived to its destination, it changes its intention and voids its target, awaiting further instructions.
	// If not, then the agent moves on the road network.
	// If it changed its road segment, ask the current road segment to register one count of traffic.
	reflex travelByCarOrFoot when: pointB != nil and not (takesTram) {
		if location = pointB.location {
			endedJourney <- true;
			pointA <- pointB;
			pointB <- nil;
			idle <- true;
			do resolveJourney;
		} else {
			do goto target: pointB on: roadNetwork;
			if speed = 5.5 * secondsPerTick {
				do updateTrafficCounters;
			}

			timeSpent <- timeSpent + secondsPerTick;
		}

	}

	// The journey by tram has multiple steps :
	// Teleport to starting stop.
	// Wait for tram.
	// Take tram.
	// Get off at destination stop, and teleport to target.
	// Signal end of journey when reached target.
	reflex travelByTram when: pointB != nil and takesTram and not (endedJourney) {

	// If not reached the tram stop from home and not inside the tram and has not taken the tram yet
	// Then that means the agent must first reach the tram stop from home. From there, when a tram approaches the stop, it launches the triggerEvaluation action.
	// See tram behavior.
		if location != chosenStopA.location and not (insideTram) {
		// Assumes travel by foot from point A to stop at roughly 1.2 meters per second.
			timeSpent <- timeSpent + (self distance_to chosenStopA) / 1.2;
			location <- chosenStopA.location;
		} else if chosenTram != nil and ((chosenTram distance_to chosenStopB) <= chosenTram.speed / 2) and insideTram {
		// If inside tram and nearing the destination stop, get off tram and move directly to destination.
		// Mark end of journey and reset all relevant variables, switch intended direction.
			ask chosenTram {
				nPassengers <- nPassengers - 1;
			}

			location <- pointB.location;
			// Assumes travel by foot from stop to pointB at roughly 1.2 meters per second.
			timeSpent <- timeSpent + (chosenStopB distance_to pointB) / 1.2;
			insideTram <- false;
			chosenTram <- nil;
			endedJourney <- true;
			pointA <- pointB;
			pointB <- nil;
			idle <- true;
			do resolveJourney;
		} else {
		// If not travelling from pointA to chosenStopA, and not arrived at chosenStopB, increase time spent.
		// Happens when in tram, but also when waiting for tram.
			timeSpent <- timeSpent + secondsPerTick;
			if insideTram {
				timeSpentInTram <- timeSpentInTram + secondsPerTick;
			} else {
				timeSpentWaiting <- timeSpentWaiting + secondsPerTick;
			}

		}

		// If the agent is inside the tram, update to location to follow the tram if not in high perf mode.
		if insideTram and not (highPerformanceMode) {
			location <- chosenTram.location;
		}

	}

	//reflex registerLocation {
	//if listLocations = [] or location != listLocations[length(listLocations) - 1] {
	//add location to: listLocations;
	//add cycle to: listCycles;
	//}

	//}
	action resolveJourney {
	// Agents spend 1 hour in a Commerce and 2 hours in a Loisirs
		if lastTarget = "commerce" {
			timeToGoHome <- hour + 1;
		}

		if lastTarget = "loisir" {
			timeToGoHome <- hour + 2;
		}

		// If they went home, then when they reach home, they end their day.
		if lastTarget = "home" {
			endedDay <- true;
		}

		// save values
		if chosenStopA != nil and chosenStopB != nil {
			save
			name + ";" + nJourney + ";" + demoClass + ";" + considersTram + ";" + transportMethod + ";" + timeSpent + ";" + timeSpentInTram + ";" + timeSpentWaiting + ";" + chosenStopA.stopName + ";" + chosenStopB.stopName + ";" + location
			to: (commuterDataOutput + string(currentYear) + ".txt") format: text rewrite: false header: false;
		} else {
			save
			name + ";" + nJourney + ";" + demoClass + ";" + considersTram + ";" + transportMethod + ";" + timeSpent + ";" + timeSpentInTram + ";" + timeSpentWaiting + ";" + ";" + ";" + location
			to: (commuterDataOutput + string(currentYear) + ".txt") format: text rewrite: false header: false;
		}

		nJourney <- nJourney + 1;
		timeSpent <- 0.0;
		timeSpentInTram <- 0.0;
		timeSpentWaiting <- 0.0;
	}

	// Increase traffic counter by 1 if it is the first the commuter sees the road it is on.
	action updateTrafficCounters {
		if current_edge != lastEdge {
			ask Roads(current_edge) {
				nPassages <- nPassages + 1;
				add cycle to: roadsListCycles;
				add nPassages to: listPassages;
			}

		}
		// Update last edge
		lastEdge <- current_edge;
	}

	// choose which tram to take for journey between pointA and pointB
	action selectTram {
		location <- pointB.location;
		closestStopsListB <- TramStops at_distance distanceToStopThreshold;
		location <- pointA.location;
		closestStopsListA <- TramStops at_distance distanceToStopThreshold;
		totalDistanceToStops <- distanceToStopThreshold * 2.0;
		chosenDirections <- [];

		// Select pair of stops based on whether or not there is a line that connects them
		// Select pair of stops that minimizes distance to stops.
		// TO ADD : select pair of stops that matches the direction chosen.
		loop stopA over: closestStopsListA {
			loop stopB over: closestStopsListB {
				if stopA.tramLines contains_any stopB.tramLines and (stopA distance_to pointA + stopB distance_to pointB) < totalDistanceToStops {
					chosenStopA <- stopA;
					chosenStopB <- stopB;
					totalDistanceToStops <- stopA distance_to pointA + stopB distance_to pointB;
				}

			}

		}

		// Select possible tram lines to take based on lines that service both chosen stops.
		if chosenStopA != nil and chosenStopB != nil and chosenStopA.stopName != chosenStopB.stopName {
			chosenTramLines <- chosenStopA.tramLines inter chosenStopB.tramLines;
		}

		loop tLine over: chosenTramLines {
			list tLineStops <- orderedStopsMap[tLine];
			chosenDirections[tLine] <- signum(tLineStops index_of chosenStopB.stopName - tLineStops index_of chosenStopA.stopName);
		}

	} }

	//*******************************************//
//************-----------------**************//
//************ MAIN EXPERIMENT **************//
//************-----------------**************//
//*******************************************//
experiment Visualization type: gui {
// Define user commands and parameters. Self-explanatory.
	user_command "Get origin" action: getOrigin category: "Define extent of addresses to model";
	user_command "Register summit location" action: register_location category: "Define extent of addresses to model";
	parameter "Show polygon?" var: drawPolygonBool category: "Define extent of addresses to model";
	user_command "Build/refresh polygon" action: build_polygon category: "Define extent of addresses to model";
	user_command "Calculate distance between first 2 points" action: calculate_distance category: "Define extent of addresses to model";
	user_command "Clear polygon and registered point list" action: clear_polygon_points category: "Define extent of addresses to model";
	user_command "Save polygon extent" action: save_polygon category: "Define extent of addresses to model";
	parameter "Saved polygon location" var: savedPolygonFilePath category: "Define extent of addresses to model";
	user_command "Load saved polygon" action: load_polygon category: "Define extent of addresses to model";
	user_command "Delete commuters outside registered area" action: kill_external_commuters category: "Define extent of addresses to model";
	parameter "Number of people aggregated into a single commuter agent" var: commuterGranularity <- 10 category: "Commuter behavior";
	parameter "Take tram when possible?" var: takeTramToggle category: "Commuter behavior";
	parameter "Proportion of adults who never take the tram" var: noTramAdultProportion <- 0.5 category: "Commuter behavior";
	parameter "Distance to stops above which commuters reject tram" var: distanceToStopThreshold category: "Commuter behavior";
	parameter "Number of vehicles for line 14" var: nTrams14 category: "Tram behavior";
	parameter "Number of vehicles for line 18" var: nTrams18 category: "Tram behavior";
	user_command "New year" action: newYear category: "Model behavior";
	parameter "Skip year at end of every commute cycle ?" var: skipYearToggle category: "Model behavior";
	parameter "Saved indicators location" var: savedIndicatorsFilePath category: "Model behavior";
	parameter "Saved roads shapefiles location" var: outputRoadsSHPFilePath category: "Model behavior";
	parameter "Saved commuter data location" var: commuterDataOutput category: "Model behavior";
	parameter "High performance mode on?" var: highPerformanceMode <- false category: "Model behavior";
	parameter "Seconds per tick" var: secondsPerTick <- 10.0 category: "Model behavior";
	parameter "Prediction mode on/off" var: predictionMode <- false category: "Prediction mode";
	parameter "Size of the new square buildings [mxm]" var: newBuildingsSize <- 15.0 category: "Prediction mode";
	parameter "Densification value : proportion of the PDC area to occupy each year" var: densificationValue <- 0.0114 category: "Prediction mode";
	parameter "Number of square meters per person" var: areaPerPerson <- 50.0 category: "Prediction mode";
	parameter "Number of stories per building" var: nStories <- 4 category: "Prediction mode";
	parameter "Percentage of new commerces built among all the new buildings inside an \"activités\" PDC zone." var: newCommercesProportion <- 0.2 category: "Prediction mode";
	parameter "Percentage of new housing built among all the new buildings inside a \"Renouvellement mixte\" PDC zone." var: mixteHousingProportion <- 0.5 category:
	"Prediction mode";
	parameter "Children proportion" var: childrenProportion category: "Demographics";
	parameter "Students proportion" var: studentsProportion category: "Demographics";
	parameter "Adults proportion" var: adultsProportion category: "Demographics";
	parameter "Retired proportion" var: retiredProportion category: "Demographics";
	// Define output.
	output {
		display Environment type: java2D {
			species PDC_areas aspect: base;
			//species Plots aspect: base;
			species Roads aspect: base;
			species TramRails aspect: base;
			species TramStops aspect: base;
			species Buildings aspect: base;
			species Trams aspect: base;
			species Trams14 aspect: base;
			species Trams18 aspect: base;
			species Commuters aspect: base;
			// Draw polygon.
			graphics "East border for addresses" {
				if drawPolygonBool {
					draw addressContainer color: #green;
				}

			}

			// Do action "get_location" whenever the user left clicks on the map.
			event mouse_down action: get_location;

			// Draw the red cross used to highlight the last location clicked. It updates on each mouse click.
			graphics "Selected Location Highlight" {
				if selected_location != nil {
					draw cross(12, 3) color: #red border: #black at: selected_location;
				}

			}

		}

		display DensityHeatmap type: java2D {
			species Plots aspect: colormap;
		}

		//display "Population" {
		//chart "Population" type: series {
		//data "Children" value: length(Commuters where (each.demoClass = "child")) color: #green;
		//data "Students" value: length(Commuters where (each.demoClass = "student")) color: #blue;
		//data "Adults" value: length(Commuters where (each.demoClass = "adult")) color: #red;
		//data "Retired" value: length(Commuters where (each.demoClass = "retired")) color: #black;
		//}

		//}

		//display "Commute time" {
		//chart "Average commute time" type: series {
		//data "Children" value: (Commuters where (each.demoClass = "child")) mean_of (each.timeSpent) color: #green;
		//data "Students" value: (Commuters where (each.demoClass = "student")) mean_of (each.timeSpent) color: #blue;
		//data "Adults" value: (Commuters where (each.demoClass = "adult")) mean_of (each.timeSpent) color: #red;
		//data "Retired" value: (Commuters where (each.demoClass = "retired")) mean_of (each.timeSpent) color: #black;
		//}

		//}

		//display "Waiting time" {
		//chart "Average waiting time" type: series {
		//data "Children" value: (Commuters where (each.demoClass = "child")) mean_of (each.timeSpentWaiting) color: #green;
		//data "Students" value: (Commuters where (each.demoClass = "student")) mean_of (each.timeSpentWaiting) color: #blue;
		//data "Adults" value: (Commuters where (each.demoClass = "adult")) mean_of (each.timeSpentWaiting) color: #red;
		//data "Retired" value: (Commuters where (each.demoClass = "retired")) mean_of (each.timeSpentWaiting) color: #black;
		//}

		//}

		// Define monitors for key variables and indicators.
		monitor "Current year" value: currentYear refresh: every(1 #cycle);
		monitor "Day number" value: day refresh: every(1 #cycle);
		monitor "Time" value: string(hour) + ":" + string(minute) refresh: every(1 #cycle);
		monitor "Number of full trams" value: length((agents of_generic_species Trams) where (each.nPassengers >= each.maxCapacity)) refresh: every(1 #cycle);
		//monitor "Percentage of commuters who take the tram" value: (100.0 * length(Commuters where (each.takesTram = true)) / length(Commuters)) with_precision 1 refresh: every(1#cycle);
	}

}

//********************************************//
//************------------------**************//
//************ HIGH PERFORMANCE **************//
//************------------------**************//
//********************************************//
experiment HighPerformance type: gui {
// Define user commands and parameters. Self-explanatory.
	user_command "Register summit location" action: register_location category: "Define extent of addresses to model";
	parameter "Show polygon?" var: drawPolygonBool category: "Define extent of addresses to model";
	user_command "Build/refresh polygon" action: build_polygon category: "Define extent of addresses to model";
	user_command "Calculate distance between first 2 points" action: calculate_distance category: "Define extent of addresses to model";
	user_command "Clear polygon and registered point list" action: clear_polygon_points category: "Define extent of addresses to model";
	user_command "Save polygon extent" action: save_polygon category: "Define extent of addresses to model";
	parameter "Saved polygon location" var: savedPolygonFilePath category: "Define extent of addresses to model";
	user_command "Load saved polygon" action: load_polygon category: "Define extent of addresses to model";
	user_command "Delete commuters outside registered area" action: kill_external_commuters category: "Define extent of addresses to model";
	parameter "Number of people aggregated into a single commuter agent" var: commuterGranularity <- 5 category: "Commuter behavior";
	parameter "Take tram when possible?" var: takeTramToggle category: "Commuter behavior";
	parameter "Distance to stops above which commuters reject tram" var: distanceToStopThreshold category: "Commuter behavior";
	parameter "Number of vehicles for line 14" var: nTrams14 category: "Tram behavior";
	parameter "Number of vehicles for line 18" var: nTrams18 category: "Tram behavior";
	user_command "New year" action: newYear category: "Model behavior";
	parameter "Skip year at end of every commute cycle ?" var: skipYearToggle category: "Model behavior";
	parameter "Saved indicators location" var: savedIndicatorsFilePath category: "Model behavior";
	parameter "Saved roads shapefiles location" var: outputRoadsSHPFilePath category: "Model behavior";
	parameter "Saved commuter data location" var: commuterDataOutput category: "Model behavior";
	parameter "High performance mode on?" var: highPerformanceMode <- true category: "Model behavior";
	parameter "Seconds per tick" var: secondsPerTick <- 60.0 category: "Model behavior";
	parameter "Prediction mode on/off" var: predictionMode <- true category: "Prediction mode";
	parameter "Size of the new square buildings [mxm]" var: newBuildingsSize <- 15.0 category: "Prediction mode";
	parameter "Densification value : proportion of the PDC area to occupy each year" var: densificationValue <- 0.0114 category: "Prediction mode";
	parameter "Number of square meters per person" var: areaPerPerson <- 50.0 category: "Prediction mode";
	parameter "Number of stories per building" var: nStories <- 4 category: "Prediction mode";
	parameter "Percentage of new commerces built among all the new buildings inside an \"activités\" PDC zone." var: newCommercesProportion <- 0.2 category: "Prediction mode";
	parameter "Percentage of new housing built among all the new buildings inside a \"Renouvellement mixte\" PDC zone." var: mixteHousingProportion <- 0.5 category:
	"Prediction mode";
	parameter "Children proportion" var: childrenProportion category: "Demographics";
	parameter "Students proportion" var: studentsProportion category: "Demographics";
	parameter "Adults proportion" var: adultsProportion category: "Demographics";
	parameter "Retired proportion" var: retiredProportion category: "Demographics";
	// Define output - no display.
	output {

	// Define monitors for key variables and indicators.
		monitor "Current year" value: currentYear refresh: every(1 #cycle);
		monitor "Day number" value: day refresh: every(1 #cycle);
		monitor "Time" value: string(hour) + ":" + string(minute) refresh: every(1 #cycle);
		monitor "Percentage of commuters who take the tram" value: (100.0 * length(Commuters where (each.takesTram = true)) / length(Commuters)) with_precision 1 refresh: every(1
		#cycle);
	}

}