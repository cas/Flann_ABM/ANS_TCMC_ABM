# -*- coding: utf-8 -*-

# Importation du module arcpy
import arcpy

arcpy.overwriteoutput = 1

# Obtenir le chemin du dossier de travail, la couche à traiter et le champ contenant les noms de types de LU.
Workspace = arcpy.GetParameterAsText(0)
layerSelected = arcpy.GetParameterAsText(1)
fieldSelected = arcpy.GetParameterAsText(2)
outputFolder = arcpy.GetParameterAsText(3)

# Créer une liste des noms de LU
listLUnames = []

# Set the current workspace
arcpy.env.workspace = Workspace

# Pour chaque ligne on récupère le nom du type de LU et on le compare à la liste. S'il n'y figure pas, on l'ajoute
with arcpy.da.SearchCursor(layerSelected, [str(fieldSelected)]) as rows:
    for row in rows:
        name = row[0]
        if name not in listLUnames:
            listLUnames.append(name)

# Ranger par ordre alphabétique comme dans ArcGIS.
listLUnames.sort()

path = outputFolder + "\ListNames_" + str(layerSelected) + "_" + str(fieldSelected) + ".csv"
# Ouvrir un fichier .csv et y copier la liste des noms pour ensuite traiter les données sur Excel par exemple
with open(path, "w") as newFile:
    for name in listLUnames:
        newFile.write(name+"\n")

