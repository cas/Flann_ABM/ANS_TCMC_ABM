# Welcome to the full document and code base for the article by Chambers et al., 2023

## Folder **activity-types**

This folder contains the ArcGIS toolbox, python script and files for the classification of activity types into the four major categories (see section 3.1.1).

## Folder **figures**

This folder contains a high resolution case study map (see section 2.2) and a screenshot of the GAMA model interface (see section 3.2).

## Folder **GAMA-model**

This folder contains the model source code (folder **models**, section 3.2) and input data (folder **includes**, section 3.1.1).


## Folder **output-datasets-analysis**

This folder contains simulation output datasets and data analysis R scripts (see section 3.3).

## Folder **TPG-schedule-data**

This folder contains departure counts for TPG lines 14 and 18 during the 2010-2023 period (see section 4.4).